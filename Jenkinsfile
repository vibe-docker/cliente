def project_token = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEF'

properties([
    gitLabConnection('gitlab_cfavacho'),
    pipelineTriggers([
        [
            $class: 'GitLabPushTrigger',
            branchFilterType: 'All',
            triggerOnPush: true,
            triggerOnMergeRequest: false,
            triggerOpenMergeRequestOnPush: "never",
            triggerOnNoteRequest: true,
            noteRegex: "Jenkins please retry a build",
            skipWorkInProgressMergeRequest: true,
            secretToken: project_token,
            ciSkip: false,
            setBuildDescription: true,
            addNoteOnMergeRequest: true,
            addCiMessage: true,
            addVoteOnMergeRequest: true,
            acceptMergeRequestOnSuccess: false,
            branchFilterType: "NameBasedFilter",
            includeBranchesSpec: "release/qat",
            excludeBranchesSpec: "",
        ]
    ])
])

pipeline {
  agent any
  stages {
    stage('Maven Install') {
      steps {
        sh 'mvn clean install -Dmaven.test.skip=true'
        sh "cp target/${IMAGE}-${VERSION}.jar /var/app_temp/${IMAGE}-${VERSION}.jar"
      }
    }
    
        
    stage('Sonar Scan') {
        steps {
            withSonarQubeEnv('sonarServer') {
                sh "mvn sonar:sonar -Dsonar.scm.disabled=true"
            }
        }
    }
        
    stage("Sonar Quality Gate"){
        steps {
            waitForQualityGate abortPipeline: true
        }
    }
    
    stage('Docker Build') {
      steps {
        withCredentials([usernamePassword(credentialsId: 'Regsitry_Nexus', passwordVariable: 'V_USER_PASS', usernameVariable: 'V_USER')]) {
            sh 'cp /var/app_temp/${IMAGE}-${VERSION}.jar ${IMAGE}-${VERSION}.jar'
            sh "sed -i 's/APP_NAME/${IMAGE}-${VERSION}.jar/g' Dockerfile "
            sh 'docker build -t ${IMAGE}:${VERSION} .'
            sh 'docker tag ${IMAGE}:${VERSION} registry.devopsvibe.com/${IMAGE}:${VERSION} '
            sh "docker login registry.devopsvibe.com -u ${V_USER} -p ${V_USER_PASS}"
            sh 'docker push registry.devopsvibe.com/${IMAGE}:${VERSION}'
        }
      }
    }
    
     stage('Anchore Scan Docker') {
        steps {
            sh "echo registry.devopsvibe.com/${IMAGE}:${VERSION} > anchore_images"
            anchore engineCredentialsId: 'anchore', engineurl: 'http://35.211.54.203:8228/v1', name: 'anchore_images'
        }
     }
     
     
    stage('Docker Registry Nexus') {
      steps {
        withCredentials([usernamePassword(credentialsId: 'Regsitry_Nexus', passwordVariable: 'V_USER_PASS', usernameVariable: 'V_USER')]) {
            sh "docker login registry.devopsvibe.com -u ${V_USER} -p ${V_USER_PASS}"
            sh 'docker push registry.devopsvibe.com/${IMAGE}:${VERSION}'
        }
      }
    }
    stage('Docker Publish') {
      parallel {
        stage('Docker Publish') {
         steps {
            sh "sed -i 's/APP_NAME/${IMAGE}/g' docker-compose.yml "
            sh "sed -i 's/APP_VERSION/${VERSION}/g' docker-compose.yml "
            sh 'docker stack deploy --with-registry-auth --compose-file docker-compose.yml spring'
          }
        }
        stage('Remove Temp App') {
          steps {
            sh 'rm -f /var/app_temp/${IMAGE}-${VERSION}.jar'
          }
        }
      }
    }
  }
  environment {
    IMAGE = readMavenPom().getArtifactId()
    VERSION = readMavenPom().getVersion()
  }
}