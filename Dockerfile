FROM anapsix/alpine-java
LABEL maintainer="devops@vibedesenv.com"
COPY target/cliente-0.0.1-SNAPSHOT.jar /home/cliente-0.0.1-SNAPSHOT.jar
CMD ["java","-Xmx128m","-jar", "-Dspring.profiles.active=cloud","/home/cliente-0.0.1-SNAPSHOT.jar"]