package cliente.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cliente.model.Cliente;


@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ClienteController {
	
	
	@GetMapping("/clientes")
	public ResponseEntity<List<Cliente>> clientes() {
		
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		clientes.add(new Cliente("47691913249", "AILTON JOSE DE SOUZA MENDES"));
		
		clientes.add(new Cliente("58885609287", "AIEZER DUARTE FILHO"));
		
	    if (!clientes.isEmpty()) {
	        return new ResponseEntity<>(clientes, HttpStatus.OK);
	    }
	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
	}

}
